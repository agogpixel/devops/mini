SimpleCov.start do
  minimum_coverage 95
  add_filter "/.git/"
end
