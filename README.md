# mini

Minimalist tool for reading INI file data.

## Usage

```bash
mini [-emt] [-n <key-name>] [-s <section>] [-v <key-value>] [-NSV] [<ini>]...
```

When no files specified, reads from stdin. When no options specified, lists ini data stripped of comments & blank lines.

### Options

| Option                                           | Description                                   |
|:------------------------------------------------:|:---------------------------------------------:|
| `-e` , `--errors`                                | Print errors.                                 |
| `-m` , `--merge`                                 | Merge duplicate sections and associated keys. |
| `-t` , `--terminate`                             | Terminate if errors.                          |
| `-n <key-name>` , `--name <key-name>`            | Search for key name.                          |
| `-s <section-name>` , `--section <section-name>` | Search within section.                        |
| `-v <key-value>` , `--value <key-value>`         | Search for key value.                         |
| `-N` , `--names`                                 | Restrict output to include key names.         |
| `-S` , `--sections`                              | Restrict output to include section names.     |
| `-V` , `--values`                                | Restrict output to include key values.        |

## Development

- Project utilizes [GitHub flow](https://guides.github.com/introduction/flow/).
