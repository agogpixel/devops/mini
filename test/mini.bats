load '/opt/bats-support/load.bash'
load '/opt/bats-assert/load.bash'

################################################################################
# Basic Usage Tests
################################################################################

@test "Prints usage and exits with error when invalid option provided." {
  source ./mini.sh

  run mini_main --fail-it

  assert_failure
  assert_output --partial "Usage:"
}

@test "Prints usage and exits with success when -h or --help." {
  source ./mini.sh

  run mini_main -h

  assert_success
  assert_output --partial "Usage:"

  run mini_main --help

  assert_success
  assert_output --partial "Usage:"
}

@test "Reads stdin when no files specified." {
  source ./mini.sh

  run mini_main < ./test/ini/empty.ini

  assert_success
  assert_output ''
}

@test "Reads file." {
  source ./mini.sh

  run mini_main ./test/ini/empty.ini

  assert_success
  assert_output ''
}

@test "Reads multiple files." {
  source ./mini.sh

  run mini_main ./test/ini/empty.ini ./test/ini/empty.ini

  assert_success
  assert_output ''
}

################################################################################
# Error Tests
################################################################################

@test "Supresses errors by default." {
  source ./mini.sh

  run mini_main ./test/ini/errors.ini

  assert_success
  assert_output '[OK]
keyB=B
keyC=C
[ALSO FAIL]'
}

@test "Displays errors with '-e' or '--errors'." {
  source ./mini.sh

  local expected='; ./test/ini/errors.ini: 1: key not in section
; ./test/ini/errors.ini: 6: invalid section name
; ./test/ini/errors.ini: 7: key not in section
; ./test/ini/errors.ini: 9: key not in section
; ./test/ini/errors.ini: 12: invalid key name
; ./test/ini/errors.ini: 14: invalid line: this will fail
[OK]
keyB=B
keyC=C
[ALSO FAIL]'

  run mini_main -e ./test/ini/errors.ini

  assert_success
  assert_output "$expected"

  run mini_main --errors ./test/ini/errors.ini

  assert_success
  assert_output "$expected"
}

@test "Terminates when errors found with '-t' or '--terminate'." {
  source ./mini.sh

  run mini_main -t ./test/ini/errors.ini

  assert_failure
  assert_output ''

  run mini_main --terminate ./test/ini/errors.ini

  assert_failure
  assert_output ''

  local expected='; ./test/ini/errors.ini: 1: key not in section
; ./test/ini/errors.ini: 6: invalid section name
; ./test/ini/errors.ini: 7: key not in section
; ./test/ini/errors.ini: 9: key not in section
; ./test/ini/errors.ini: 12: invalid key name
; ./test/ini/errors.ini: 14: invalid line: this will fail'

  run mini_main -e -t ./test/ini/errors.ini

  assert_failure
  assert_output "$expected"

  run mini_main --errors --terminate ./test/ini/errors.ini

  assert_failure
  assert_output "$expected"
}

################################################################################
# Basic Parse Tests
################################################################################

@test "Concatenates multiple files by default." {
  source ./mini.sh

  run mini_main ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output '[A]
a=default
b=default
c=default
[B]
[C]
[D]
a=default
b=default
c=default
[A]
b=dev
c=dev
[B]
a=dev
b=dev
c=dev
[C]
[E]
a=dev
b=dev
c=dev
[A]
c=stage
[B]
[C]
a=stage
b=stage
c=stage
[F]
a=stage
b=stage
c=stage
[A]
[B]
a=prod
b=prod
c=prod
[C]
a=prod
b=prod
c=prod
[G]
a=prod
b=prod
c=prod'
}

@test "Combines multiple files with '-m' or '--merge'." {
  source ./mini.sh

  local expected='[A]
a=default
b=dev
c=stage
[B]
a=prod
b=prod
c=prod
[C]
a=prod
b=prod
c=prod
[D]
a=default
b=default
c=default
[E]
a=dev
b=dev
c=dev
[F]
a=stage
b=stage
c=stage
[G]
a=prod
b=prod
c=prod'

  run mini_main -m ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Section Search Tests
################################################################################

@test "Searches within section with '-s <section>' or '--section <section>' (no merge)." {
  source ./mini.sh

  local expected='[A]
a=default
b=default
c=default
[A]
b=dev
c=dev
[A]
c=stage
[A]'

  run mini_main -s A ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with '-s <section>' or '--section <section>' (with merge)." {
  source ./mini.sh

  local expected='[A]
a=default
b=dev
c=stage'

  run mini_main -m -s A ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with '-s <section>' or '--section <section>' (no merge)." {
  source ./mini.sh

  local expected='[A]
a=default
b=default
c=default
[C]
[A]
b=dev
c=dev
[C]
[A]
c=stage
[C]
a=stage
b=stage
c=stage
[A]
[C]
a=prod
b=prod
c=prod'

  run mini_main -s A -s C ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --section C ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with '-s <section>' or '--section <section>' (with merge)." {
  source ./mini.sh

  local expected='[A]
a=default
b=dev
c=stage
[C]
a=prod
b=prod
c=prod'

  run mini_main -m -s A -s C ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --section C ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Key Name Search Tests
################################################################################

@test "Searches for key name with '-n <key-name>' or '--name <key-name>' (no merge)." {
  source ./mini.sh

  local expected='[A]
a=default
[D]
a=default
[B]
a=dev
[E]
a=dev
[C]
a=stage
[F]
a=stage
[B]
a=prod
[C]
a=prod
[G]
a=prod'

  run mini_main -n a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --name a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key name with '-n <key-name>' or '--name <key-name>' (with merge)." {
  source ./mini.sh

  local expected='[A]
a=default
[B]
a=prod
[C]
a=prod
[D]
a=default
[E]
a=dev
[F]
a=stage
[G]
a=prod'

  run mini_main -m -n a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --name a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key names with '-n <key-name>' or '--name <key-name>' (no merge)." {
  source ./mini.sh

  local expected='[A]
a=default
c=default
[D]
a=default
c=default
[A]
c=dev
[B]
a=dev
c=dev
[E]
a=dev
c=dev
[A]
c=stage
[C]
a=stage
c=stage
[F]
a=stage
c=stage
[B]
a=prod
c=prod
[C]
a=prod
c=prod
[G]
a=prod
c=prod'

  run mini_main -n a -n c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --name a --name c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key names with '-n <key-name>' or '--name <key-name>' (with merge)." {
  source ./mini.sh

  local expected='[A]
a=default
c=stage
[B]
a=prod
c=prod
[C]
a=prod
c=prod
[D]
a=default
c=default
[E]
a=dev
c=dev
[F]
a=stage
c=stage
[G]
a=prod
c=prod'

  run mini_main -m -n a -n c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --name a --name c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Key Value Search Tests
################################################################################

@test "Searches for key value with '-v <key-value>' or '--value <key-value>' (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=stage
b=stage
c=stage
[F]
a=stage
b=stage
c=stage'

  run mini_main -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key value with '-v <key-value>' or '--value <key-value>' (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[F]
a=stage
b=stage
c=stage'

  run mini_main -m -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key values with '-v <key-value>' or '--value <key-value>' (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=stage
b=stage
c=stage
[F]
a=stage
b=stage
c=stage
[B]
a=prod
b=prod
c=prod
[C]
a=prod
b=prod
c=prod
[G]
a=prod
b=prod
c=prod'

  run mini_main -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key values with '-v <key-value>' or '--value <key-value>' (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[B]
a=prod
b=prod
c=prod
[C]
a=prod
b=prod
c=prod
[F]
a=stage
b=stage
c=stage
[G]
a=prod
b=prod
c=prod'

  run mini_main -m -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Section & Key Name Search Tests
################################################################################

@test "Searches within section with key name (no merge)." {
  source ./mini.sh

  local expected='[A]
a=default'

  run mini_main -s A -n a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --name a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key name (with merge)." {
  source ./mini.sh

  local expected='[A]
a=default'

  run mini_main -m -s A -n a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --name a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key names (no merge)." {
  source ./mini.sh

  local expected='[A]
a=default
c=default
[A]
c=dev
[A]
c=stage'

  run mini_main -s A -n a -n c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --name a --name c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key names (with merge)." {
  source ./mini.sh

  local expected='[A]
a=default
c=stage'

  run mini_main -m -s A -n a -n c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --name a --name c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key name (no merge)." {
  source ./mini.sh

  local expected='[A]
a=default
[C]
a=stage
[C]
a=prod'

  run mini_main -s A -s C -n a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --section C --name a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key name (with merge)." {
  source ./mini.sh

  local expected='[A]
a=default
[C]
a=prod'

  run mini_main -m -s A -s C -n a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --section C --name a ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key names (no merge)." {
  source ./mini.sh

  local expected='[A]
a=default
c=default
[A]
c=dev
[A]
c=stage
[C]
a=stage
c=stage
[C]
a=prod
c=prod'

  run mini_main -s A -s C -n a -n c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --section C --name a --name c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key names (with merge)." {
  source ./mini.sh

  local expected='[A]
a=default
c=stage
[C]
a=prod
c=prod'

  run mini_main -m -s A -s C -n a -n c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --section C --name a --name c ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Section & Key Value Search Tests
################################################################################

@test "Searches within section with key value (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -s A -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key value (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -m -s A -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key values (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -s A -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key values (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -m -s A  -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key value (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=stage
b=stage
c=stage'

  run mini_main -s A -s C -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --section C --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key value (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -m -s A -s C -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --section C --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key values (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=stage
b=stage
c=stage
[C]
a=prod
b=prod
c=prod'

  run mini_main -s A -s C -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --section C --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key values (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=prod
b=prod
c=prod'

  run mini_main -m -s A -s C -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --section C --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Key Name & Key Value Search Tests
################################################################################

@test "Searches for key name with key value (no merge)." {
  source ./mini.sh

  local expected='[C]
a=stage
[F]
a=stage'

  run mini_main -n a -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --name a --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key name with key value (with merge)." {
  source ./mini.sh

  local expected='[F]
a=stage'

  run mini_main -m -n a -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --name a --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key name with key values (no merge)." {
  source ./mini.sh

  local expected='[C]
a=stage
[F]
a=stage
[B]
a=prod
[C]
a=prod
[G]
a=prod'

  run mini_main -n a -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --name a --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key name with key values (with merge)." {
  source ./mini.sh

  local expected='[B]
a=prod
[C]
a=prod
[F]
a=stage
[G]
a=prod'

  run mini_main -m -n a -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --name a --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key names with key value (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=stage
c=stage
[F]
a=stage
c=stage'

  run mini_main -n a -n c -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --name a --name c --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key names with key value (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[F]
a=stage
c=stage'

  run mini_main -m -n a -n c -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --name a --name c --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key names with key values (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=stage
c=stage
[F]
a=stage
c=stage
[B]
a=prod
c=prod
[C]
a=prod
c=prod
[G]
a=prod
c=prod'

  run mini_main -n a -n c -v stage -v prod  ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --name a --name c --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches for key names with key values (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[B]
a=prod
c=prod
[C]
a=prod
c=prod
[F]
a=stage
c=stage
[G]
a=prod
c=prod'

  run mini_main -m -n a -n c -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --name a --name c --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Section & Key Name & Key Value Search Tests
################################################################################

@test "Searches within section with key name & key value (no merge)." {
  source ./mini.sh

  local expected=''

  run mini_main -s A -n a -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --name a --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key name & key value (with merge)." {
  source ./mini.sh

  local expected=''

  run mini_main -m -s A -n a -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --name a --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key names & key value (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -s A -n a -n c -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --name a --name c --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key names & key value (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -m -s A -n a -n c -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --name a --name c --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key name & key values (no merge)." {
  source ./mini.sh

  local expected=''

  run mini_main -s A -n a -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --name a --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key name & key values (with merge)." {
  source ./mini.sh

  local expected=''

  run mini_main -m -s A -n a -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --name a --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key names & key values (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -s A -n a -n c -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --name a --name c --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within section with key names & key values (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -m -s A -n a -n c -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --name a --name c --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key name & key value (no merge)." {
  source ./mini.sh

  local expected='[C]
a=stage'

  run mini_main -s A -s C -n a -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --section C --name a --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key name & key value (with merge)." {
  source ./mini.sh

  local expected=''

  run mini_main -m -s A -s C -n a -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --section C --name a --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key names & key value (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=stage
c=stage'

  run mini_main -s A -s C -n a -n c -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --section C --name a --name c --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key names & key value (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage'

  run mini_main -m -s A -s C -n a -n c -v stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --section C --name a --name c --value stage ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key name & key values (no merge)." {
  source ./mini.sh

  local expected='[C]
a=stage
[C]
a=prod'

  run mini_main -s A -s C -n a -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --section C --name a --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key name & key values (with merge)." {
  source ./mini.sh

  local expected='[C]
a=prod'

  run mini_main -m -s A -s C -n a -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --section C --name a --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key names & key values (no merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=stage
c=stage
[C]
a=prod
c=prod'

  run mini_main -s A -s C -n a -n c -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --section A --section C --name a --name c --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

@test "Searches within sections with key names & key values (with merge)." {
  source ./mini.sh

  local expected='[A]
c=stage
[C]
a=prod
c=prod'

  run mini_main -m -s A -s C -n a -n c -v stage -v prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --section A --section C --name a --name c --value stage --value prod ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Section Filter Tests
################################################################################

@test "Displays section headers with '-S' or '--sections' (no merge)." {
  source ./mini.sh

  run mini_main -S ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output '[A]
[B]
[C]
[D]
[A]
[B]
[C]
[E]
[A]
[B]
[C]
[F]
[A]
[B]
[C]
[G]'
}

@test "Displays section headers with '-S' or '--sections' (with merge)." {
  source ./mini.sh

  local expected='[A]
[B]
[C]
[D]
[E]
[F]
[G]'

  run mini_main -m -S ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --sections ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Key Name Filter Tests
################################################################################

@test "Displays key names with '-N' or '--names' (no merge)." {
  source ./mini.sh

  run mini_main -N ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output 'a
b
c
a
b
c
b
c
a
b
c
a
b
c
c
a
b
c
a
b
c
a
b
c
a
b
c
a
b
c'
}

@test "Displays key names with '-N' or '--names' (with merge)." {
  source ./mini.sh

  local expected='a
b
c
a
b
c
a
b
c
a
b
c
a
b
c
a
b
c
a
b
c'

  run mini_main -m -N ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --names ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Key Value Filter Tests
################################################################################

@test "Displays key values with '-V' or '--values' (no merge)." {
  source ./mini.sh

  run mini_main -V ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output 'default
default
default
default
default
default
dev
dev
dev
dev
dev
dev
dev
dev
stage
stage
stage
stage
stage
stage
stage
prod
prod
prod
prod
prod
prod
prod
prod
prod'
}

@test "Displays key values with '-V' or '--values' (with merge)." {
  source ./mini.sh

  local expected='default
dev
stage
prod
prod
prod
prod
prod
prod
default
default
default
dev
dev
dev
stage
stage
stage
prod
prod
prod'

  run mini_main -m -V ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --values ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Key Name & Key Value Filter Tests
################################################################################

@test "Display key names & key values (no merge)." {
  source ./mini.sh

  run mini_main -NV ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output 'a=default
b=default
c=default
a=default
b=default
c=default
b=dev
c=dev
a=dev
b=dev
c=dev
a=dev
b=dev
c=dev
c=stage
a=stage
b=stage
c=stage
a=stage
b=stage
c=stage
a=prod
b=prod
c=prod
a=prod
b=prod
c=prod
a=prod
b=prod
c=prod'
}

@test "Display key names & key values (with merge)." {
  source ./mini.sh

  local expected='a=default
b=dev
c=stage
a=prod
b=prod
c=prod
a=prod
b=prod
c=prod
a=default
b=default
c=default
a=dev
b=dev
c=dev
a=stage
b=stage
c=stage
a=prod
b=prod
c=prod'

  run mini_main -m -NV ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --names --values ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Section & Key Name Filter Tests
################################################################################

@test "Display sections & key names (no merge)." {
  source ./mini.sh

  run mini_main -NS ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output '[A]
a
b
c
[B]
[C]
[D]
a
b
c
[A]
b
c
[B]
a
b
c
[C]
[E]
a
b
c
[A]
c
[B]
[C]
a
b
c
[F]
a
b
c
[A]
[B]
a
b
c
[C]
a
b
c
[G]
a
b
c'
}

@test "Display sections & key names (with merge)." {
  source ./mini.sh

  local expected='[A]
a
b
c
[B]
a
b
c
[C]
a
b
c
[D]
a
b
c
[E]
a
b
c
[F]
a
b
c
[G]
a
b
c'

  run mini_main -m -NS ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --names --sections ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Section & Key Value Filter Tests
################################################################################

@test "Display sections & key values (no merge)." {
  source ./mini.sh

  run mini_main -SV ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output '[A]
default
default
default
[B]
[C]
[D]
default
default
default
[A]
dev
dev
[B]
dev
dev
dev
[C]
[E]
dev
dev
dev
[A]
stage
[B]
[C]
stage
stage
stage
[F]
stage
stage
stage
[A]
[B]
prod
prod
prod
[C]
prod
prod
prod
[G]
prod
prod
prod'
}

@test "Display sections & key values (with merge)." {
  source ./mini.sh

  local expected='[A]
default
dev
stage
[B]
prod
prod
prod
[C]
prod
prod
prod
[D]
default
default
default
[E]
dev
dev
dev
[F]
stage
stage
stage
[G]
prod
prod
prod'

  run mini_main -m -SV ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --sections --values ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}

################################################################################
# Section & Key Name & Key Value Filter Tests
################################################################################

@test "Display sections & key names & key values (no merge)." {
  source ./mini.sh

  run mini_main -NSV ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output '[A]
a=default
b=default
c=default
[B]
[C]
[D]
a=default
b=default
c=default
[A]
b=dev
c=dev
[B]
a=dev
b=dev
c=dev
[C]
[E]
a=dev
b=dev
c=dev
[A]
c=stage
[B]
[C]
a=stage
b=stage
c=stage
[F]
a=stage
b=stage
c=stage
[A]
[B]
a=prod
b=prod
c=prod
[C]
a=prod
b=prod
c=prod
[G]
a=prod
b=prod
c=prod'
}

@test "Display sections & key names & key values (with merge)." {
  source ./mini.sh

  local expected='[A]
a=default
b=dev
c=stage
[B]
a=prod
b=prod
c=prod
[C]
a=prod
b=prod
c=prod
[D]
a=default
b=default
c=default
[E]
a=dev
b=dev
c=dev
[F]
a=stage
b=stage
c=stage
[G]
a=prod
b=prod
c=prod'

  run mini_main -m -NSV ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"

  run mini_main --merge --names --sections --values ./test/ini/default.ini ./test/ini/dev.ini ./test/ini/stage.ini ./test/ini/prod.ini

  assert_success
  assert_output "$expected"
}
