#!/bin/bash

set -o errexit
set -o pipefail
set -o noclobber
set -o nounset
#set -o xtrace

################################################################################
# Runtime Settings
################################################################################

# Track options found.
declare -A MINI_OPTIONS=()

# User provided sections.
declare -a MINI_TARGET_SECTIONS=()

# User provided key names.
declare -a MINI_TARGET_KEY_NAMES=()

# User provided key values.
declare -a MINI_TARGET_KEY_VALUES=()

# User provided INI files. Default to stdin.
MINI_INI_SOURCES=-

################################################################################
# Options
################################################################################

# Print help.
declare -A MINI_OPT_HELP=([NAME]=help [LONG]=help [SHORT]=h)

# Search within section.
declare -A MINI_OPT_SECTION=([NAME]=section [LONG]=section [SHORT]=s)

# Search for key name.
declare -A MINI_OPT_KEY_NAME=([NAME]=key-name [LONG]=name [SHORT]=n)

# Search for key value.
declare -A MINI_OPT_KEY_VALUE=([NAME]=key-value [LONG]=value [SHORT]=v)

# Restrict output to include section headers.
declare -A MINI_OPT_SECTIONS=([NAME]=sections [LONG]=sections [SHORT]=S)

# Restrict output to include key names.
declare -A MINI_OPT_KEY_NAMES=([NAME]=key-names [LONG]=names [SHORT]=N)

# Restrict output to include key values.
declare -A MINI_OPT_KEY_VALUES=([NAME]=key-values [LONG]=values [SHORT]=V)

# Merge duplicate sections and associated keys.
declare -A MINI_OPT_MERGE=([NAME]=merge [LONG]=merge [SHORT]=m)

# Print errors.
declare -A MINI_OPT_ERRORS=([NAME]=errors [LONG]=errors [SHORT]=e)

# Terminate if errors.
declare -A MINI_OPT_TERMINATE=([NAME]=terminate [LONG]=terminate [SHORT]=t)

################################################################################
# Parser Regular Expressions
################################################################################

# $1 - Line.
# $2 - Comment.
readonly MINI_PARSER_BLANK_OR_COMMENT_REGEX='^([[:space:]]*|[[:space:]]*;(.*))$'

# $1 - Section name.
readonly MINI_PARSER_SECTION_NAME_REGEX='^[[:space:]]*\[(.*)\][[:space:]]*$'

# $1 - Key name.
# $2 - Key value.
readonly MINI_PARSER_KEY_REGEX='^([^=]*)=(.*)$'

# $1 - Sanitized section name.
readonly MINI_PARSER_SANITIZE_SECTION_NAME_REGEX='^[[:space:]]*([^[:space:]]|[^[:space:]].*[^[:space:]])[[:space:]]*$'

# $1 - Sanitized key name.
readonly MINI_PARSER_SANITIZE_KEY_NAME_REGEX='^[[:space:]]*([^[:space:]]|[^[:space:]].*[^[:space:]])[[:space:]]*$'

# $1 - Sanitized key value.
readonly MINI_PARSER_SANITIZE_KEY_VALUE_REGEX='^[[:space:]]*([^[:space:]]|[^[:space:]].*[^[:space:]])[[:space:]]*$'

# No bounding whitespace & not empty string.
readonly MINI_PARSER_VALIDATE_SECTION_NAME_REGEX='^([^[:space:]]|[^[:space:]].*[^[:space:]])$'

# No bounding whitespace & not empty string.
readonly MINI_PARSER_VALIDATE_KEY_NAME_REGEX='^([^[:space:]]|[^[:space:]].*[^[:space:]])$'

################################################################################
# Parser Data Structures
################################################################################

# Track order of parser errors.
declare -a MINI_PARSER_ERRORS=()

# Track current ini source.
MINI_PARSER_CURRENT_INI_SOURCE=

# Track ini line number.
MINI_PARSER_CURRENT_INI_LINE_NUMBER=

# Parsed (sanitized & validated) ini data, in order.
declare -a MINI_PARSER_PARSED_INI_DATA=()

# Track current section name.
MINI_PARSER_CURRENT_SECTION_NAME=

################################################################################
# Merger Data Structures
################################################################################

# Merged ini data, in order.
declare -a MINI_MERGER_MERGED_INI_DATA=()

################################################################################
# Reducer Data Structures
################################################################################

# Reduced ini data, in order.
declare -a MINI_REDUCER_REDUCED_INI_DATA=()

# Track reducer's index into merged data.
MINI_REDUCER_CURRENT_INDEX=

################################################################################
# Main Function
################################################################################

mini_main() {
  mini_parse_args $@
  mini_parse_ini_sources
  mini_handle_ini_error_state
  mini_merge_ini
  mini_reduce_ini
  mini_print_ini
}

################################################################################
# Argument Handler Functions
################################################################################

mini_parse_args() {
  local opts="${MINI_OPT_HELP[SHORT]}${MINI_OPT_ERRORS[SHORT]}${MINI_OPT_MERGE[SHORT]}${MINI_OPT_TERMINATE[SHORT]}${MINI_OPT_KEY_NAME[SHORT]}:${MINI_OPT_SECTION[SHORT]}:${MINI_OPT_KEY_VALUE[SHORT]}:${MINI_OPT_KEY_NAMES[SHORT]}${MINI_OPT_SECTIONS[SHORT]}${MINI_OPT_KEY_VALUES[SHORT]}"
  local longopts="${MINI_OPT_HELP[LONG]},${MINI_OPT_ERRORS[LONG]},${MINI_OPT_MERGE[LONG]},${MINI_OPT_TERMINATE[LONG]},${MINI_OPT_KEY_NAME[LONG]}:,${MINI_OPT_SECTION[LONG]}:,${MINI_OPT_KEY_VALUE[LONG]}:,${MINI_OPT_KEY_NAMES[LONG]},${MINI_OPT_SECTIONS[LONG]},${MINI_OPT_KEY_VALUES[LONG]}"

  ! PARSED=$(getopt --options=$opts --longoptions=$longopts --name "$0" -- "$@")

  if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has complained about wrong arguments to stdout.
    mini_print_usage
    exit 1
  fi

  # Handle quoting in getopt output.
  eval set -- "$PARSED"

  while true; do
    case "$1" in
      "-${MINI_OPT_HELP[SHORT]}"|"--${MINI_OPT_HELP[LONG]}")
        mini_print_help
        exit 0
        ;;
      "-${MINI_OPT_ERRORS[SHORT]}"|"--${MINI_OPT_ERRORS[LONG]}")
        mini_set_errors_option
        shift
        ;;
      "-${MINI_OPT_MERGE[SHORT]}"|"--${MINI_OPT_MERGE[LONG]}")
        mini_set_merge_option
        shift
        ;;
      "-${MINI_OPT_TERMINATE[SHORT]}"|"--${MINI_OPT_TERMINATE[LONG]}")
        mini_set_terminate_option
        shift
        ;;
      "-${MINI_OPT_KEY_NAME[SHORT]}"|"--${MINI_OPT_KEY_NAME[LONG]}")
        mini_set_key_name_option "$2"
        shift 2
        ;;
      "-${MINI_OPT_SECTION[SHORT]}"|"--${MINI_OPT_SECTION[LONG]}")
        mini_set_section_option "$2"
        shift 2
        ;;
      "-${MINI_OPT_KEY_VALUE[SHORT]}"|"--${MINI_OPT_KEY_VALUE[LONG]}")
        mini_set_key_value_option "$2"
        shift 2
        ;;
      "-${MINI_OPT_KEY_NAMES[SHORT]}"|"--${MINI_OPT_KEY_NAMES[LONG]}")
        mini_set_key_names_option
        shift 1
        ;;
      "-${MINI_OPT_SECTIONS[SHORT]}"|"--${MINI_OPT_SECTIONS[LONG]}")
        mini_set_sections_option
        shift 1
        ;;
      "-${MINI_OPT_KEY_VALUES[SHORT]}"|"--${MINI_OPT_KEY_VALUES[LONG]}")
        mini_set_key_values_option
        shift 1
        ;;
      --)
        shift
        break
        ;;
    esac
  done

  if [ "$#" -gt 0 ]; then
    # File(s).
    MINI_INI_SOURCES="$@"
  fi
}

mini_set_errors_option() {
  MINI_OPTIONS["${MINI_OPT_ERRORS[NAME]}"]=1
}

mini_is_errors_option_set() {
  [ -v 'MINI_OPTIONS[${MINI_OPT_ERRORS[NAME]}]' ]
}

mini_set_merge_option() {
  MINI_OPTIONS["${MINI_OPT_MERGE[NAME]}"]=1
}

mini_is_merge_option_set() {
  [ -v 'MINI_OPTIONS[${MINI_OPT_MERGE[NAME]}]' ]
}

mini_set_terminate_option() {
  MINI_OPTIONS["${MINI_OPT_TERMINATE[NAME]}"]=1
}

mini_is_terminate_option_set() {
  [ -v 'MINI_OPTIONS[${MINI_OPT_TERMINATE[NAME]}]' ]
}

mini_set_key_name_option() {
  local key_name="$(mini_sanitize_key_name "$1")"

  if mini_validate_key_name "$key_name"; then
    MINI_OPTIONS["${MINI_OPT_KEY_NAME[NAME]}"]=1
    MINI_TARGET_KEY_NAMES+=("$key_name")
  else
    MINI_PARSER_ERRORS+=("invalid key name option")
  fi
}

mini_is_key_name_option_set() {
  [ -v 'MINI_OPTIONS[${MINI_OPT_KEY_NAME[NAME]}]' ]
}

mini_set_section_option() {
  local section_name="$(mini_sanitize_section_name "$1")"

  if mini_validate_section_name "$section_name"; then
    MINI_OPTIONS["${MINI_OPT_SECTION[NAME]}"]=1
    MINI_TARGET_SECTIONS+=("$section_name")
  else
    MINI_PARSER_ERRORS+=("invalid section name option")
  fi
}

mini_is_section_option_set() {
  [ -v 'MINI_OPTIONS[${MINI_OPT_SECTION[NAME]}]' ]
}

mini_set_key_value_option() {
  MINI_OPTIONS["${MINI_OPT_KEY_VALUE[NAME]}"]=1
  MINI_TARGET_KEY_VALUES+=("$(mini_sanitize_key_value "$1")")
}

mini_is_key_value_option_set() {
  [ -v 'MINI_OPTIONS[${MINI_OPT_KEY_VALUE[NAME]}]' ]
}

mini_set_key_names_option() {
  MINI_OPTIONS["${MINI_OPT_KEY_NAMES[NAME]}"]=1
}

mini_is_key_names_option_set() {
  [ -v 'MINI_OPTIONS[${MINI_OPT_KEY_NAMES[NAME]}]' ]
}

mini_set_sections_option() {
  MINI_OPTIONS["${MINI_OPT_SECTIONS[NAME]}"]=1
}

mini_is_sections_option_set() {
  [ -v 'MINI_OPTIONS[${MINI_OPT_SECTIONS[NAME]}]' ]
}

mini_set_key_values_option() {
  MINI_OPTIONS["${MINI_OPT_KEY_VALUES[NAME]}"]=1
}

mini_is_key_values_option_set() {
  [ -v 'MINI_OPTIONS[${MINI_OPT_KEY_VALUES[NAME]}]' ]
}

mini_is_targeted() {
  local type="$1"
  local candidate="$2"

  local targets=

  case "$type" in
    section)
      targets=( "${MINI_TARGET_SECTIONS[@]}" )
      ;;
    key-name)
      targets=( "${MINI_TARGET_KEY_NAMES[@]}" )
      ;;
    key-value)
      targets=( "${MINI_TARGET_KEY_VALUES[@]}" )
      ;;
  esac

  for target in "${targets[@]}"; do
    if [ "$candidate" = "$target" ]; then
      return 0
    fi
  done

  return 1
}

################################################################################
# INI Parser Functions
################################################################################

mini_parse_ini_sources() {
  for ini in $MINI_INI_SOURCES; do
    MINI_PARSER_CURRENT_INI_SOURCE="$ini"
    mini_parse_ini < <(cat -- $MINI_PARSER_CURRENT_INI_SOURCE)
  done
}

mini_parse_ini() {
  MINI_PARSER_CURRENT_INI_LINE_NUMBER=0

  while IFS= read -r line; do
    ((++MINI_PARSER_CURRENT_INI_LINE_NUMBER))

    if [[ "$line" =~ $MINI_PARSER_BLANK_OR_COMMENT_REGEX ]]; then
      # Empty line or comment.
      :
    elif [[ "$line" =~ $MINI_PARSER_SECTION_NAME_REGEX ]]; then
      # Section element.
      mini_parse_ini_section_name "${BASH_REMATCH[1]}"
    elif [[ "$line" =~ $MINI_PARSER_KEY_REGEX ]]; then
      # Key element.
      mini_parse_ini_key "${BASH_REMATCH[1]}" "${BASH_REMATCH[2]}"
    else
      MINI_PARSER_ERRORS+=("$MINI_PARSER_CURRENT_INI_SOURCE: $MINI_PARSER_CURRENT_INI_LINE_NUMBER: invalid line: $line")
    fi
  done
}

mini_parse_ini_section_name() {
  local section_name="$(mini_sanitize_section_name "$1")"

  if mini_validate_section_name "$section_name"; then
    MINI_PARSER_CURRENT_SECTION_NAME="$section_name"
    MINI_PARSER_PARSED_INI_DATA+=("[$section_name]")
  else
    MINI_PARSER_CURRENT_SECTION_NAME=
    MINI_PARSER_ERRORS+=("$MINI_PARSER_CURRENT_INI_SOURCE: $MINI_PARSER_CURRENT_INI_LINE_NUMBER: invalid section name")
  fi
}

mini_parse_ini_key() {
  local key_name="$(mini_sanitize_key_name "$1")"
  local key_value="$(mini_sanitize_key_value "$2")"

  if [ -z "$MINI_PARSER_CURRENT_SECTION_NAME" ]; then
    MINI_PARSER_ERRORS+=("$MINI_PARSER_CURRENT_INI_SOURCE: $MINI_PARSER_CURRENT_INI_LINE_NUMBER: key not in section")
  elif mini_validate_key_name "$key_name"; then
    MINI_PARSER_PARSED_INI_DATA+=("$key_name=$key_value")
  else
    MINI_PARSER_ERRORS+=("$MINI_PARSER_CURRENT_INI_SOURCE: $MINI_PARSER_CURRENT_INI_LINE_NUMBER: invalid key name")
  fi
}

################################################################################
# Error Handler Functions
################################################################################

mini_handle_ini_error_state() {
  if mini_is_errors_option_set && mini_has_errors; then
    mini_print_errors
  fi

  if mini_is_terminate_option_set && mini_has_errors; then
    exit 1
  fi
}

mini_has_errors() {
  [ "${#MINI_PARSER_ERRORS[@]}" -gt 0 ]
}

################################################################################
# INI Merger Functions
################################################################################

mini_merge_ini() {
  if ! mini_is_merge_option_set; then
    for element in "${MINI_PARSER_PARSED_INI_DATA[@]}"; do
      MINI_MERGER_MERGED_INI_DATA+=("$element")
    done

    return
  fi

  declare -A section_merged=()

  for element in "${MINI_PARSER_PARSED_INI_DATA[@]}"; do
    if [[ "$element" =~ $MINI_PARSER_SECTION_NAME_REGEX ]]; then
      local section_name="${BASH_REMATCH[1]}"

      if [ ! -v 'section_merged[$section_name]' ]; then
        MINI_MERGER_MERGED_INI_DATA+=("[$section_name]")

        while read -r key; do
          MINI_MERGER_MERGED_INI_DATA+=("$key")
        done << EOF
$(mini_merge_ini_section "$section_name")
EOF

        section_merged["$section_name"]=1
      fi
    fi
  done
}

mini_merge_ini_section() {
  local section_name="$1"

  declare -a key_names_in_order=()
  declare -A key_value_by_key_name=()
  declare -A key_names_found=()

  local in_section=

  for element in "${MINI_PARSER_PARSED_INI_DATA[@]}" ]; do
    if [[ "$element" =~ $MINI_PARSER_SECTION_NAME_REGEX ]]; then
      if [ "${BASH_REMATCH[1]}" = "$section_name" ]; then
        in_section=1
      else
        in_section=
      fi
    elif [[ ! -z "$in_section" && "$element" =~ $MINI_PARSER_KEY_REGEX ]]; then
      local key_name="${BASH_REMATCH[1]}"
      local key_value="${BASH_REMATCH[2]}"

      if [ ! -v 'key_names_found[$key_name]' ]; then
        key_names_in_order+=("$key_name")
        key_names_found["$key_name"]=1
      fi

      key_value_by_key_name["$key_name"]="$key_value"
    fi
  done

  for key_name in "${key_names_in_order[@]}"; do
    printf '%s=%s\n' "$key_name" "${key_value_by_key_name[$key_name]}"
  done
}

################################################################################
# INI Reducer Functions
################################################################################

mini_reduce_ini() {
  MINI_REDUCER_CURRENT_INDEX=0

  while [ "$MINI_REDUCER_CURRENT_INDEX" -lt "${#MINI_MERGER_MERGED_INI_DATA[@]}" ]; do
    if [[ "${MINI_MERGER_MERGED_INI_DATA[$MINI_REDUCER_CURRENT_INDEX]}" =~ $MINI_PARSER_SECTION_NAME_REGEX ]]; then
      local section_name="${BASH_REMATCH[1]}"

      if mini_is_section_option_set && ! mini_is_targeted section "$section_name"; then
        # Cull section.
        mini_reduce_ini_cull_section
        continue
      fi

      if ! mini_is_key_name_option_set && ! mini_is_key_value_option_set; then
        # Include section.
        mini_reduce_ini_include_section "$section_name"
        continue
      fi

      # Include if criteria met.
      mini_reduce_ini_section_with_criteria "$section_name"
    fi
  done
}

mini_reduce_ini_cull_section() {
  while [[ $((++MINI_REDUCER_CURRENT_INDEX)) -lt "${#MINI_MERGER_MERGED_INI_DATA[@]}" && ! "${MINI_MERGER_MERGED_INI_DATA[$MINI_REDUCER_CURRENT_INDEX]}" =~ $MINI_PARSER_SECTION_NAME_REGEX ]]; do
    # Skip keys.
    continue
  done
}

mini_reduce_ini_include_section() {
  local section_name="$1"

  MINI_REDUCER_REDUCED_INI_DATA+=("[$section_name]")

  while [[ $((++MINI_REDUCER_CURRENT_INDEX)) -lt "${#MINI_MERGER_MERGED_INI_DATA[@]}" && "${MINI_MERGER_MERGED_INI_DATA[$MINI_REDUCER_CURRENT_INDEX]}" =~ $MINI_PARSER_KEY_REGEX ]]; do
    # Include keys.
    MINI_REDUCER_REDUCED_INI_DATA+=("${MINI_MERGER_MERGED_INI_DATA[$MINI_REDUCER_CURRENT_INDEX]}")
  done
}

mini_reduce_ini_section_with_criteria() {
  local section_name="$1"

  declare -a key_buffer=()

  while [[ $((++MINI_REDUCER_CURRENT_INDEX)) -lt "${#MINI_MERGER_MERGED_INI_DATA[@]}" && "${MINI_MERGER_MERGED_INI_DATA[$MINI_REDUCER_CURRENT_INDEX]}" =~ $MINI_PARSER_KEY_REGEX ]]; do
    # Only include if keys match criteria.
    local key="${MINI_MERGER_MERGED_INI_DATA[$MINI_REDUCER_CURRENT_INDEX]}"

    local key_name="${BASH_REMATCH[1]}"
    local key_value="${BASH_REMATCH[2]}"

    if mini_is_key_name_option_set && mini_is_key_value_option_set; then
      if mini_is_targeted key-name "$key_name" && mini_is_targeted key-value "$key_value"; then
        key_buffer+=("$key")
      fi
    elif mini_is_key_name_option_set; then
      if mini_is_targeted key-name "$key_name"; then
        key_buffer+=("$key")
      fi
    elif mini_is_key_value_option_set; then
      if mini_is_targeted key-value "$key_value"; then
        key_buffer+=("$key")
      fi
    fi
  done

  if [ "${#key_buffer[@]}" -gt 0 ]; then
    # Include section.
    MINI_REDUCER_REDUCED_INI_DATA+=("[$section_name]")

    for key in "${key_buffer[@]}"; do
      # Include keys.
      MINI_REDUCER_REDUCED_INI_DATA+=("$key")
    done
  fi
}

################################################################################
# Print Functions
################################################################################

mini_print_ini() {
  local print_all=

  if ! mini_is_sections_option_set && ! mini_is_key_names_option_set && ! mini_is_key_values_option_set; then
    print_all=1
  fi

  for element in "${MINI_REDUCER_REDUCED_INI_DATA[@]}"; do
    if [ ! -z "$print_all" ]; then
      printf '%s\n' "$element"
    elif mini_is_sections_option_set && [[ "$element" =~ $MINI_PARSER_SECTION_NAME_REGEX ]]; then
      printf '%s\n' "$element"
    elif [[ "$element" =~ $MINI_PARSER_KEY_REGEX ]]; then
      local key_name="${BASH_REMATCH[1]}"
      local key_value="${BASH_REMATCH[2]}"

      if mini_is_key_names_option_set && mini_is_key_values_option_set; then
        printf '%s\n' "$element"
      elif mini_is_key_names_option_set; then
        printf '%s\n' "$key_name"
      elif mini_is_key_values_option_set; then
        printf '%s\n' "$key_value"
      fi
    fi
  done
}

mini_print_usage() {
  cat <<EOF
Usage:
  $0 [-${MINI_OPT_ERRORS[SHORT]}${MINI_OPT_MERGE[SHORT]}${MINI_OPT_TERMINATE[SHORT]}] [-${MINI_OPT_KEY_NAME[SHORT]} <key-name>] [-${MINI_OPT_SECTION[SHORT]} <section>] [-${MINI_OPT_KEY_VALUE[SHORT]} <key-value>] [-${MINI_OPT_KEY_NAMES[SHORT]}${MINI_OPT_SECTIONS[SHORT]}${MINI_OPT_KEY_VALUES[SHORT]}] [<ini>]...

  When no files specified, reads from stdin. When no options specified, lists
  ini data stripped of comments & blank lines.

  -${MINI_OPT_ERRORS[SHORT]}, --${MINI_OPT_ERRORS[LONG]}
    Print errors.

  -${MINI_OPT_MERGE[SHORT]}, --${MINI_OPT_MERGE[LONG]}
    Merge duplicate sections and associated keys, last duplicate taking
    precedence.

  -${MINI_OPT_TERMINATE[SHORT]}, --${MINI_OPT_TERMINATE[LONG]}
    Terminate if errors.

  -${MINI_OPT_KEY_NAME[SHORT]} <key-name>, --${MINI_OPT_KEY_NAME[LONG]} <key-name>
    Search for key name.

  -${MINI_OPT_SECTION[SHORT]} <section>, --${MINI_OPT_SECTION[LONG]} <section>
    Search within section.

  -${MINI_OPT_KEY_VALUE[SHORT]} <key-value>, --${MINI_OPT_KEY_VALUE[LONG]} <key-value>
    Search for key value.

  -${MINI_OPT_KEY_NAMES[SHORT]}, --${MINI_OPT_KEY_NAMES[LONG]}
    Restrict output to include key names.

  -${MINI_OPT_SECTIONS[SHORT]}, --${MINI_OPT_SECTIONS[LONG]}
    Restrict output to include section headers.

  -${MINI_OPT_KEY_VALUES[SHORT]}, --${MINI_OPT_KEY_VALUES[LONG]}
    Restrict output to include key values.
EOF
}

mini_print_help() {
  printf '\nMinimalist tool for reading INI file data.\n\n'
  mini_print_usage
  printf '\nSee https://gitlab.com/agogpixel/devops/mini for more info.\n'
}

mini_print_errors() {
  printf '; %s\n' "${MINI_PARSER_ERRORS[@]}"
}

################################################################################
# Sanitization & Validation Functions
################################################################################

mini_sanitize_key_name() {
  if [[ "$1" =~ $MINI_PARSER_SANITIZE_KEY_NAME_REGEX ]]; then
    printf '%s' "${BASH_REMATCH[1]}"
  fi
}

mini_validate_key_name() {
  [[ "$1" =~ $MINI_PARSER_VALIDATE_KEY_NAME_REGEX ]]
}

mini_sanitize_section_name() {
  if [[ "$1" =~ $MINI_PARSER_SANITIZE_SECTION_NAME_REGEX ]]; then
    printf '%s' "${BASH_REMATCH[1]}"
  fi
}

mini_validate_section_name() {
  [[ "$1" =~ $MINI_PARSER_VALIDATE_SECTION_NAME_REGEX ]]
}

mini_sanitize_key_value() {
  if [[ "$1" =~ $MINI_PARSER_SANITIZE_KEY_VALUE_REGEX ]]; then
    printf '%s' "${BASH_REMATCH[1]}"
  fi
}

################################################################################
# Program Entry
################################################################################

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  mini_main $@
fi
